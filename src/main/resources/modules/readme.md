# Ringo-Kit

Ringo-Kit is an Atlassian Plugins 3 Universal Binary runtime "kit" that leverages [RingoJS](http://ringojs.org/) as the underlying web framework. P3 kits allow Atlassian Plugin developers a choice in frameworks and languages on the JVM to use in building their plugins. P3 kits compile down to a jar that can be deployed remotely on it's own server or inside of a running Atlassian product.

Here's what a simple Ringo-Kit plugin looks like:

    var app = exports.app = require("atlassian/app").create();

    app.configure({
      stylesheets: ["app"],
      scripts: ["app"]
    });

    app.all("/index", function (req, res) {
      res.send('Hello World!');
    });

## Features

- Bundles RingoJs for a mature set of libraries
- Wrapping modules to expose services in ideomatic JavaScript
- OSGi manifest generation assistence via a hard dependency on common packages
- Automatic CoffeeScript support
- Includes Handlebars and Underscore

## Writing a Ringo-Kit plugin

In order to build a Ringo-Kit plugin, you'll first need to install the AP3 tools to get you started. You can do that by running this in your console:

    curl https://bitbucket.org/atlassian/ap3-sdk-bin/raw/master/ap3-setup.sh | sh

Once you've installed AP3, you can issue the following command to create your first plugin:

    ap3 new ringo-kit

You'll be asked which template you'd like to use. Choose the [basic] template. This will also ask you to enter the name and namespace you'd like to use for your plugin. Once you do that, it'll create your plugin inside `./<name-of-your-plugin>`.

    cd <name-of-your-plugin>

Open your favorite text editor or IDE and browse through the code.

### Choice between JavaScript or CoffeeScript

The basic template provides you two ways to code your plugin: JavaScript or [CoffeeScript](http://coffeescript.org/). In the root of your plugin directory, you'll find `main.js` and `main.coffee`. These files are the entry point into your plugin. Ringo-Kit will use  `main.js` if it exists, but will look for `main.coffee` if it doesn't. The template provides both so you can make a choice in what you want to write your plugin with -- just delete the one you don't want to use.

### Running your plugin

To run your plugin, you have a few choices. Ringo-Kit is a Universal Binary plugin kit which means it can be deployed remotely on your own server and consumed by an OnDemand instance or your own JIRA or Confluence running behind a firewall.

#### Deploying to RJC

The fastest way to get up and running is to just run your plugin against <https://remoteapps.jira.com>. This is the OnDemand instance for the Atlassian Plugins 3 project. To deploy your plugin to this instance, you'll need to [register for an account](https://remoteapps.jira.com/secure/Signup!default.jspa) then email <mailto:developer-relations@atlassian.com> to be registered as developer -- this will gain you access to deploy onto RJC.

Once you have access, running your plugin inside of RJC is as simple as:

    ap3 start p3

The first time you run your plugin, you will be asked if you would like to register it with a remote host. It will provide you a default ([rjc]) -- select that then enter your username and password for RJC. After you do that, AP3 will run your plugin inside the Plugins 3 container. It will do the following:

1. Make your local web server available on the internet by tunneling it through a "local tunnel" service running on **lt39.info**
2. Register your plugin into RJC
3. Launch your browser to RJC

From there, look for your plugin under the "More" dropdown in the header.

*INSERT IMAGE HERE*

Your new plugin, should look something like this:

*INSERT IMAGE HERE*

#### Deploying to a local JIRA or Confluence instance

You can use AP3 to start up a local JIRA or Confluence instance like so:

    ap3 start [jira|confluence]

Once you've started a local JIRA or Confluence, you can start the P3 container in another console window:

    ap3 start p3

AP3 will detect that you have a local JIRA or Confluence instance running and will automatically register your plugin for you.

### Usage outside of AP3

To use this kit, add this to your Maven project:

    <dependency>
      <groupId>com.atlassian.pluginkit</groupId>
      <artifactId>ringojs-kit</artifactId>
      <version>LATEST</version>
    </dependency>

To see a simple example, look at the 'app/' directory.


## Reporting issues

Issues are hosted on [Bitbucket](https://bitbucket.org/atlassian/ringojs-kit/issues?status=new&status=open). If you find a bug, [report it](https://bitbucket.org/atlassian/ringojs-kit/issues/new).

## Contributing

The Ringo-Kit code is hosted at [Bitbucket](https://bitbucket.org/atlassian/ringojs-kit): <https://bitbucket.org/atlassian/ringojs-kit>

Pull requests are very welcome. All we ask is that if you add something to the API, please provide the documentation with it. Also, right now, tests are slim to none -- please add tests with your features.

## Development


To use this kit, add this to your Maven project:

    <dependency>
      <groupId>com.atlassian.pluginkit</groupId>
      <artifactId>ringojs-kit</artifactId>
      <version>LATEST</version>
    </dependency>

To see a simple example, look at the 'app/' directory.


## Acknowledgments

* [Don Brown](https://twitter.com/mrdonbrown)
* [Bob Bergman](https://twitter.com/rbergman)
* [Rich Manalang](https://twitter.com/rmanalan)
* Jonathan Doklovic

## License

Apache License v2. See [LICENSE.txt](https://bitbucket.org/atlassian/ringojs-kit/src/master/LICENSE.txt)
