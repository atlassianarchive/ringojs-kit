J = JavaImporter(
  com.atlassian.util.concurrent.Promises,
  com.atlassian.util.concurrent.Promise,
  com.google.common.util.concurrent.SettableFuture
)
{proxy} = require "./util"

# Promises / Deferred Mixin
#
#     var Deferred = require("atlassian/promises").Deferred;
#
# @mixin

Promises =

  # Creates a promise. Provides a way to execute callback functions based on one or more objects,
  # usually Deferred objects that represent asynchronous events.
  # @param [Deferreds] first The first of 1-n promise arguments, or an array of promises
  # @return [Promise] promise A promise representing the state of all promise arguments
  when: (first) ->
    args = if first instanceof Array then first else arguments
    J.Promises.when args...

  # Alias of 'when' for use coffeescript where 'when' is a keyword.
  all: -> exports.when arguments...

  # Creates a promise for an existing value (aliases `resolved`).
  # @param [Object] value The value for which a promise should be returned
  promise: (value) -> J.Promises.promise value

  # Creates a promise for existing content (aliases `promise`).
  # @param [Object] value The value for which a promise should be returned
  resolved: (value) -> exports.promise value

  # Creates a promise for an existing Error or Throwable.
  # @param [Error] t The error or Java Throwable for which a promise should be returned
  rejected: (t) -> Promises.rejected t

  # A constructor function that returns a chainable utility object with methods to register
  # multiple callbacks into callback queues, invoke callback queues, and relay the success
  # or failure state of any synchronous or asynchronous function.
  Deferred: ->
    future = J.SettableFuture.create()
    proxy future,
      resolve: (value) -> future.set value; @
      reject: (t) -> future.setException t; @
      promise: -> J.Promises.forFuture future

exports = module.exports = Promises
