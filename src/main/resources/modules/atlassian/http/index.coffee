# EXPERIMENTAL

encode = encodeURIComponent

op = (method) ->
  (url, opts={}) ->
    if typeof url isnt "string" then [opts, url] = [url, url?.url]
    throw new Error "HTTP requests require a url" if not url?
    subtype = "#{if /^http[s]?:/.test url then '' else 'host-'}client"
    http = require "atlassian/http/#{subtype}"
    req = http.newRequest url
    req.setHeader k, v for k, v of opts.headers if opts.headers
    if opts.body?
      body =
        if opts.body and typeof opts.body is "object"
          if req.contentType is "application/json"
            JSON.stringify opts.body
          else if req.contentType is "application/x-www-form-urlencoded"
            ("#{encode k}=#{encode v.toString()}" for k, v of opts.body when v?).join "&"
          else
            opts.body.toString()
        else
          opts.body
      req.setEntity body
    promise = req[method]()
    xform = promise.transform()
    for k, v of opts
      n = parseInt k
      if n > 0
        xform = xform.on n, v
      else if k in xformers
        if v.length? and v.length > 0
          xform = xform[k] f for f in v
        else
          xform = xform[k] v
    promise = xform.toPromise()
    for k, v of opts when k in callbacks
      if v.length? and v.length > 0
        promise = promise[k] f for f in v
      else
        promise = promise[k] v
    promise

xformers = [
  "informational"
  "successful"
  "ok"
  "created"
  "noContent"
  "redirection"
  "seeOther"
  "notModified"
  "clientError"
  "badRequest"
  "unauthorized"
  "forbidden"
  "notFound"
  "conflict"
  "serverError"
  "internalServerError"
  "serviceUnavailable"
  "error"
  "notSuccessful"
  "others"
  "otherwise"
]

callbacks = [
  "done"
  "fail"
  "then"
  "map"
  "flatMap"
  "recover"
]

exports[method] = op method for method in [
  "get"
  "post"
  "put"
  "delete"
  "head"
  "trace"
  "options"
]

exports.del = exports.delete;
