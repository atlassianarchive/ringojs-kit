{proxy} = require "atlassian/util"

# BigPipe provides an API to allow non-blocking page load for
# situations where you need to execute a blocking operation,
# such as http calls.
#
# ## Example
#
# The following is an example of a simple plugin registered at `/index`
# that retrieves lorem ipsum data from a remote service 4 times, then
# returns each response asyncronously through BigPipe.
#
#     var app = exports.app = require("atlassian/app").create();
#
#     var http = require("atlassian/http/client");
#     var {range} = require("vendor/underscore");
#
#     app.get("/index", function (req, res) {
#       var bigpipe = res.bigpipe;
#       res.render("index", {
#         loripsums: range(4).map(function (i) {
#           var promise =
#             http.newRequest("http://loripsum.net/api").get()
#               .transform()
#               .ok(function (response) response.getEntity())
#               .otherwise(function (t) "<pre>" + t + "</pre>")
#               .toPromise();
#           return {
#             html: bigpipe.promiseHtmlContent(promise).initialContent,
#             index: i + 1
#           };
#         })
#       });
#     });
#
# The corresponding view `index.hbs` referenced in the `render` method above:
#
#     <div class="aui-group">
#     {{#each loripsums}}
#       <div class="aui-item">
#         <h3>Random #{{index}}</h3>
#         {{{html}}}
#       </div>
#     {{/each}}
#     </div>
#
# @see https://remoteapps.jira.com/wiki/display/ARA/Using+BigPipe+in+P3+plugins
# @see https://bitbucket.org/rbergman/xhr-bigpipe-plugin
# @see https://bitbucket.org/rbergman/loripsum-plugin
class BigPipeManager

  # No need to create a BigPipeManager instance. This module creates a new instance when required.
  constructor: ->
    return appContext.getBean "bigPipeManager"

  # The following methods/properties are just used by the documentation generator

  # Gets the bigpipe instance for the current request; for use in providing bigpipe content.
  # @return [BigPipe] A bigpipe instance
  # @throws IllegalStateException If called from a thread lacking an assigned requestId
  getBigPipe: ->

  # Gets the consumable bigpipe instance for the current request; for use in consuming bigpipe content.
  # @return [Option<ConsumableBigPipe>] An option for a consumable bigpipe instance
  getConsumableBigPipe: ->

module.exports = new BigPipeManager
