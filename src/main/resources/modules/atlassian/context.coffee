context = appContext.getBean "renderContext"

{mash} = require "vendor/underscore"
{proxy} = require "atlassian/util"

module.exports = do ->
  proxy context,
    toJSON: ->
      mash ([e.key, e.value] for e in context.toContextMap().entrySet().toArray())

    toArray: ->
      context.toContextMap().entrySet().toArray().map (o) ->
        key: o.key
        value: o.value

# The following class is just provided so that the code can be documented through codo.

# Provides global access to common properties and the i18n interface for use in
# your view templates. You can access them in [Handlebars](http://handlebarsjs.com) templates like so:
#
#     <h1>Hello {{userId}}</h1>
#
class Context
  # @property [String] a base64 encoded string containing the client key and user id. You can use this to construct an Authorization request header when you need to make ajax requests back to the plugin container.
  authState: null

  # @property [String] indicates whether or not bigpipe has had content promised to it for the current request
  bigPipeActivated: null

  # @property [String] the id the bigpipe system should use to make its content polling requests with
  bigPipeRequestId: null

  # @property [String] the client key of the host application
  clientKey: null

  # @property [String] the base url of the host resource (e.g., https://remoteapps.jira.com/remotable-plugins)
  hostBaseResourceUrl: null

  # @property [String] the host's base url (e.g., https://remoteapps.jira.com)
  hostBaseUrl: null

  # @property [String] the url for the host's script url (e.g., https://remoteapps.jira.com/remotable-plugins/all.js)
  hostScriptUrl: null

  # @property [String] the url for the host's css url (e.g., https://remoteapps.jira.com/remotable-plugins/all.css)
  hostStylesheetUrl: null

  # @property [String] the context path of the host (e.g., /wiki)
  hostContextPath: null

  # Responsible for resolving a message or key/argument pairs to their internationalized message.
  # See the [Javadoc](https://developer.atlassian.com/static/javadoc/sal/2.3/reference/com/atlassian/sal/api/message/I18nResolver.html)
  # @return [com.atlassian.sal.api.message.I18nResolver]
  i18n: ->

  # @property [String] the base url of the local plugin container
  localBaseUrl: null

  # @property [String] the locale of the browser
  locale: null

  # @property [String] the user id of the logged in user
  userId: null